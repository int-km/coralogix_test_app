require 'concurrent'
require 'json'
require 'uri'
require 'net/http'
require 'date'

CORALOGIX_URL='https://api.coralogix.com/api/v1/logs'
TOKEN=ENV['CORALOGIX_TOKEN']

class Coralogix

  def initialize( _app_name, _subsystem_name, _period=1, _max_size=10  )
    @app_name, @subsystem_name, @period, @max_size = _app_name, _subsystem_name, _period, _max_size
    @logs = Concurrent::Array.new
    Thread.new { periodic_task }
  end

  def periodic_task
    while true
      sleep @period
      send_logs @logs
      @logs = Concurrent::Array.new
    end
  end

  def log (_log)
    @logs << _log
    if @logs.size >= @max_size
      send_logs @logs
      @logs = Concurrent::Array.new
    end
  end

  private

  def send_request(data)
    uri = URI.parse(CORALOGIX_URL)
    header = {'Content-Type' => 'application/json'}
    http = Net::HTTP.new uri.host, uri.port
    http.use_ssl = true
    request = Net::HTTP::Post.new uri.request_uri, header
    request.body = data.to_json
    http.request request
  end

  def send_logs(_logs)

    return if _logs.empty?
    Thread.new do
      begin
      payload = {privateKey: TOKEN,
                 applicationName: @app_name,
                 subsystemName: @subsystem_name,
                 logEntries: _logs.map{|log| {timestamp: DateTime.now.strftime('%Q'), severity: 1, text: log} }
      }
      response = send_request payload
      puts 'response is: '+response.body+" for #{payload[:logEntries].size} lines"
      rescue => e
        puts e.message
      end
    end
  end

end