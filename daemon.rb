

# this app simply generates logs with random period and sends it to coralogix.com
# author email:gura.andrey@gmail.com skype:sb.ahura

require './coralogix.rb'

def random_log
  log_to_send = ''
  rand(2..20).times do
    word = ''
    rand(2..20).times do
      word += ('a'..'z').to_a[rand(25)]
    end
    log_to_send += "#{word.capitalize!} "
  end
  log_to_send.chomp+"\n"
end

coralogix = Coralogix.new 'coralogix_test_app', 'main daemon', 5

while true
  sleep rand 0.1
  log = random_log
  puts "randomlog is: #{log}"
  coralogix.log log
end
